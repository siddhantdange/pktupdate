'use strict';

const Config = require('../../config');

const Bluebird = require('bluebird');
const Linkedin = Bluebird.promisifyAll(require('node-linkedin')(Config.LINKEDIN_ID, Config.LINKEDIN_SECRET, Config.LINKEDIN_REDIRECT_URI));

exports.getAuthLink = (nonce) => {
    const scope = ['r_basicprofile', 'r_emailaddress'];

    return Linkedin.auth.authorize(scope, nonce);
};

exports.getProfile = (access_token, profile_id) => {
  const linkedin = Linkedin.init(access_token);

  if (!linkedin) {
    return {};
  }

  linkedin.people.me = Bluebird.promisify(linkedin.people.me);

  return linkedin.people.me()
  .catch((err) => {
    return {};
  })
};

exports.profileFromUrl = (access_token, url) => {
  const linkedin = Linkedin.init(access_token);

  console.log(linkedin);
  if (!linkedin) {
    return {};
  }

  linkedin.people.me = Bluebird.promisify(linkedin.people.me);

  return linkedin.people.me()
  .then((p) => {
    console.log(p);
    return p;
  });
};

'use strict';

const Util = require('util');

const Config = require('../config');

const Linkedin = require('./libraries/linkedin');

const AccessToken = require('./models/access_token');
const Brother = require('./models/brother');



  Util.log(`PKTUpdate started on port:${Config.PORT}`);
  const updated = {};
  let token;

  return new AccessToken().fetch()
  .then((access_token) => {
    token = access_token.get('token');
    console.log(token);

    const urls = {
      'https://www.linkedin.com/in/adeterry/'	: 'ade	terry',
      'https://www.linkedin.com/in/avincenthill/'	: 'alex	vincent-hill',
      'https://www.linkedin.com/in/andrew-lowe-50b14349/'	: 'andrew	lowe',
      'https://www.linkedin.com/in/andy-steinberg-cfa-09963921/'	: 'andy	steinberg',
      'https://www.linkedin.com/in/austin-reuter-00346155/'	: 'austin	reuter',
      'https://www.linkedin.com/in/briansherman03/'	: 'brian	sherman',
      'https://www.linkedin.com/in/brianjkong/'	: 'brian	kong',
      'https://www.linkedin.com/in/connor-anderson-729371a1/'	: 'connor	anderson',
      'https://www.linkedin.com/in/daltonboll/'	: 'dalton 	boll',
      'https://www.linkedin.com/in/danielyanga/'	: 'daniel	yang',
      'https://www.linkedin.com/in/gtjohnson22/'	: 'greg	johnson',
      'https://www.linkedin.com/in/hardikagrawal/'	: 'hardik	agrawal',
      'https://www.linkedin.com/in/ian-hill-b1845952/'	: 'ian	hill',
      'https://www.linkedin.com/in/jackpardini/'	: 'jack	pardini',
      'https://www.linkedin.com/in/jihwan-owh-85ab5932/'	: 'jihwan	owh',
      'https://www.linkedin.com/in/jfan13/'	: 'jimmy	fan',
      'https://www.linkedin.com/in/josephmelo/'	: 'joseph	mello',
      'https://www.linkedin.com/in/justinwang22/'	: 'justin	wang',
      'https://www.linkedin.com/in/justinwang22/'	: 'kevin	tanaka',
      'https://www.linkedin.com/in/kotarowalsh/'	: 'kotaro	walsh',
      'https://www.linkedin.com/in/michaelcguss/'	: 'michael	guss',
      'https://www.linkedin.com/in/nicholas-zobrist-17811b76/'	: 'nicholas	zobrist',
      'https://www.linkedin.com/in/nicholas-snyder-75a36755/'	: 'nick	snyder',
      'https://www.linkedin.com/in/paulshefelton/'	: 'paul	sheflton',
      'https://www.linkedin.com/in/ryantsinclair/'	: 'ryan	sinclair',
      'https://www.linkedin.com/in/seanschen1/'	: 'sean	chen',
      'https://www.linkedin.com/in/stefankatz1/'	: 'stefan	katz',
      'https://www.linkedin.com/in/sujay-rau-10606b31/'	: 'sujay	rau',
      'https://www.linkedin.com/in/sumedhb/'	: 'sumedh	bhattacharya',
      'https://www.linkedin.com/in/vincenttzeng/'	: 'vincent	tzeng',
      'https://www.linkedin.com/in/zachary-burford-39111597/'	: 'zach	burford',
      'https://www.linkedin.com/in/darienlo/'	: 'darien	lo',
      'https://www.linkedin.com/in/adambspaulding/'	: 'adamp	spaulding',
      'https://www.linkedin.com/in/gabriel-valenzuela-111372128/'	: 'gabe	valenzuela',
      'https://www.linkedin.com/in/jmspark/'	: 'james	park',
      'https://www.linkedin.com/in/kongjames/'	: 'james	kong',
    };

    Object.keys(urls).forEach((url) => {
      const name = urls[url];

      Linkedin.profileFromUrl(token, url)
      .then((profile) => {
        console.log(profile)
        console.log(`${name}:${profile.id}`);
      })
      .catch((err) => {
        console.log(`ERROR: ${err}`);
      })
    });

  //});
  // .map((brother) => {
  //   return Linkedin.getProfile(token, brother.linkedin_id)
  //   .then((profile) => {
  //     const updated = {
  //       jobtype: profile.industry;
  //     };
  //
  //     if (profile.emailAddress) {
  //       updated.email = profile.emailAddress;
  //     }
  //
  //     if (profile && profile.positions && profile.positions._total) {
  //       updated.job = profile.positions.values[0].title;
  //       updated.company = profile.positions.values[0].company.name;
  //     }
  //
  //     return new Brother({ id: brother.id }).save(updated).fetch();
  //   })
  //   .then((brother) => {
  //     if (Date.now() - brother.last_modifed < 1) {
  //       updated[brother.id] = 1;
  //     }
  //   });
  // })
  // .then(() => {
  //   const updateCount = Object.keys(updated).length;
  //   Util.log(`PKTUpdate successfully updated ${updatedCount} entries`);
  // });
});

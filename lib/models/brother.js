'use strict';

const Bookshelf = require('../libraries/bookshelf');

module.exports = Bookshelf.Model.extend({
  tableName: 'brothers',
  filter: function (filter) {
    filter = filter || {};

    return this.query((qb) => {
      if (filter.id) {
        qb.where('id', filter.id);
      }

      if (filter.firstname) {
        qb.where('firstname', filter.firstname);
      }

      if (filter.lastname) {
        qb.where('lastname', filter.lastname);
      }

      if (filter.job) {
        qb.where('job', filter.job);
      }

      if (filter.company) {
        qb.where('company', filter.company);
      }

      if (filter.jobtype) {
        qb.where('jobtype', filter.jobtype);
      }

      if (filter.email) {
        qb.where('email', filter.email);
      }

      if (filter.phone) {
        qb.where('phone', filter.phone);
      }

      if (filter.linkedin_id) {
        qb.where('linkedin_id', filter.linkedin_id);
      }

    });
  },
  serialize: function () {
    return {
      id: this.get('id'),
      firstname: this.get('firstname'),
      lastname: this.get('lastname'),
      job: this.get('job'),
      company: this.get('company'),
      jobtype: this.get('jobtype'),
      email: this.get('email'),
      phone: this.get('phone'),
      linkedin_id: this.get('linkedin_id'),
      object: 'brother'
    }
  }
});

'use strict';

const Bookshelf = require('../libraries/bookshelf');

module.exports = Bookshelf.Model.extend({
  tableName: 'access_tokens',
  idAttribute: ['token'],
  filter: function (filter) {
    filter = filter || {};

    return this.query((qb) => {

      if (filter.id) {
        qb.where('id', filter.id);
      }

      if (filter.date_created) {
        if (filter.date_created.lt) {
          qb.where('date_created', '<', filter.date_created.lt);
        } else if (filter.date_created.lte) {
          qb.where('date_created', filter.date_created.lte).orWhere('date_created', '<', filter.date_created.lte);
        }

        if (filter.date_created.gt) {
          qb.where('date_created', '>', filter.date_created.gt);
        } else if (filter.date_created.gte) {
          qb.where('date_created', filter.date_created.gte).orWhere('date_created', '>', filter.date_created.gte);
        }
      }
    });
  },
  serialize: function () {
    return {
      token: this.get('token'),
      date_created: this.get('date_created')
    }
  }
});

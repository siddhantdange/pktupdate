'use strict';

const Joi = require('joi');

const BrotherValidator = require('../../../lib/validators/brothers/brother.js');

describe('brother validator', () => {

  describe('firstname', () => {

    it('is required', () => {
      const payload = {};
      const result = Joi.validate(payload, BrotherValidator);

      expect(result.error.details[0].path).to.eql('firstname');
      expect(result.error.details[0].type).to.eql('any.required');
    });

    it('is not empty', () => {
      const payload = { firstname: '' };
      const result = Joi.validate(payload, BrotherValidator);

      expect(result.error.details[0].path).to.eql('firstname');
      expect(result.error.details[0].type).to.eql('any.empty');
    });

    it('is less than 255 characters', () => {
      const payload = { firstname: 'a'.repeat(260) };
      const result = Joi.validate(payload, BrotherValidator);

      expect(result.error.details[0].path).to.eql('firstname');
      expect(result.error.details[0].type).to.eql('string.max');
    });

  });

  describe('email', () => {

    it('is contains @ sign', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'incorrect',
        phone: '1'.repeat(10),
        linkedin_id: 'test'
      };
      const result = Joi.validate(payload, BrotherValidator);

      expect(result.error.details[0].path).to.eql('email');
      expect(result.error.details[0].type).to.eql('string.email');
    });

  });

  describe('phone', () => {

    it('is greater than 10 digits', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(11),
        linkedin_id: 'test'
      };
      const result = Joi.validate(payload, BrotherValidator);

      expect(result.error.details[0].path).to.eql('phone');
      expect(result.error.details[0].type).to.eql('string.max');
    });

    it('is less than 10 digits', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(9),
        linkedin_id: 'test'
      };
      const result = Joi.validate(payload, BrotherValidator);

      expect(result.error.details[0].path).to.eql('phone');
      expect(result.error.details[0].type).to.eql('string.min');
    });

  });

});

'use strict';

const Brother = require('../../lib/models/brother');

describe('brother model', () => {

  describe('serialize', () => {

    it('includes all of the necessary fields', () => {
      const brother = Brother.forge().serialize();

      expect(brother).to.have.all.keys([
        'id',
        'firstname',
        'lastname',
        'job',
        'company',
        'jobtype',
        'email',
        'phone',
        'linkedin_id',
        'object',
      ]);
    });

  });

});

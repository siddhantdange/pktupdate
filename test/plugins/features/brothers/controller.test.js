'use strict';

const Brother    = require('../../../../lib/models/brother.js');
const Controller = require('../../../../lib/plugins/features/brothers/controller.js');

describe('brothers controller', () => {

  describe('findAll', () => {

    it('gets all brothers', () => {
      return Controller.findAll()
      .then((brothers) => {
        expect(brothers.length).to.be.at.least(0);
      });
    });

    it('filters by firstname', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(10),
        linkedin_id: 'test'
      };

      return Controller.create(payload)
      .then((brother) => {
        const id = brother.get('id');
        const filter = Object.assign({ id }, payload);

        return Controller.findAll(filter)
        .then((brothers) => {
          expect(brothers.length).to.eql(1);
          expect(brothers.models[0].id).to.equal(id);
        });
      })
    });

  });

  describe('create', () => {

    it('creates a brother', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(10),
        linkedin_id: 'test'
      };

      return Controller.create(payload)
      .then((brother) => {
        expect(brother.get('firstname')).to.eql(payload.firstname);

        return new Brother({ id: brother.id }).fetch();
      })
      .then((brother) => {
        expect(brother.get('email')).to.eql(payload.email);
        expect(brother.get('phone')).to.eql(payload.phone);
      });
    });

    it('updates a brother', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(10),
        linkedin_id: 'test'
      };

      return Controller.create(payload)
      .then((brother) => {

        payload.firstname = 'test2';
        return Controller.update(brother.id, payload);
      })
      .then((brother) => {
        expect(brother.get('firstname')).to.eql(payload.firstname);
      });
    });

  });

});

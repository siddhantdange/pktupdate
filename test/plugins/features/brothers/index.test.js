'use strict';

const PKTAlumni = require('../../../../lib/server');

describe('movies integration', () => {

  describe('create', () => {

    it('creates a brother', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(10),
        linkedin_id: 'test'
      };

      return PKTAlumni.inject({
        url: '/brothers',
        method: 'POST',
        payload
      })
      .then((response) => {
        expect(response.statusCode).to.eql(200);
        expect(response.result.object).to.eql('brother');
      });
    });

    it('updates a brother', () => {
      const payload = {
        firstname: 'test',
        lastname: 'test',
        job: 'test',
        company: 'test',
        jobtype: 'test',
        email: 'correct@valid.com',
        phone: '1'.repeat(10),
        linkedin_id: 'test'
      };

      return PKTAlumni.inject({
        url: '/brothers',
        method: 'POST',
        payload
      })
      .then((response) => {
        payload.firstname = 'test2';

        return PKTAlumni.inject({
          url: `/brothers/${response.result.id}`,
          method: 'POST',
          payload
        })
      })
      .then((response) => {
        expect(response.statusCode).to.eql(200);
        expect(response.result.object).to.eql('brother');
        expect(response.result.firstname).to.eql(payload.firstname);
      })
    });

  });

  describe('findAll', () => {

    it('finds all brothers', () => {
      return PKTAlumni.inject({
        url: '/brothers',
        method: 'GET'
      })
      .then((response) => {
        expect(response.statusCode).to.eql(200);
        response.result.forEach((brother) => expect(brother.object).to.eql('brother'));
      });
    });

  });

});

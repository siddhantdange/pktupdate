'use strict';

module.exports = {
  DB_HOST: 'localhost',
  DB_NAME: 'pkt_alumni_test',
  DB_PASSWORD: '',
  DB_PORT: 5432,
  DB_USER: 'pkt_alumni_user',
  PORT: '3000'
};

'use strict';

module.exports = {
  DB_HOST: 'localhost',
  DB_NAME: 'pkt_alumni_development',
  DB_PASSWORD: '',
  DB_PORT: 5432,
  DB_USER: 'pkt_alumni_user',
  FRONTEND_HOST: 'http://localhost:8888',
  LINKEDIN_ID: '',
  LINKEDIN_REDIRECT_URI: 'http://e85caae2.ngrok.io/v1/linkedin/auth/callback',
  LINKEDIN_SECRET: '',
  PORT: '3000'
};
